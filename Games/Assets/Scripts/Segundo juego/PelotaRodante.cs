using UnityEngine;

public class PelotaRodante : MonoBehaviour
{
    public float velocidadRotacion = 100f;
    public float velocidadMovimiento = 5f;

    private void Update()
    {
        // Rotar la pelota constantemente en vertical
        transform.Rotate(Vector3.up, velocidadRotacion * Time.deltaTime);

        // Movimiento horizontal del personaje
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * movimientoHorizontal * velocidadMovimiento * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Comprobar si la colisión fue con una bala
        if (collision.gameObject.CompareTag("Bala"))
        {
            // Desactivar gravedad y cinemática de la bala
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.isKinematic = true;

            // Hacer que la bala se convierta en hijo de la pelota
            collision.transform.SetParent(transform);
        }
    }
}