using UnityEngine;
using UnityEngine.UI;

public class DisparoPersonaje : MonoBehaviour
{
    public GameObject proyectilPrefab;
    public Transform puntoDisparo;
    public float fuerzaDisparo = 10f;
    public Text gameOverText;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Disparar();
        }
    }

    private void Disparar()
    {
        GameObject proyectil = Instantiate(proyectilPrefab, puntoDisparo.position, puntoDisparo.rotation);
        Rigidbody rb = proyectil.GetComponent<Rigidbody>();

        // Aplicar fuerza al proyectil en la direcci�n del personaje (derecha)
        rb.AddForce(transform.right * fuerzaDisparo, ForceMode.Impulse);

    }


}