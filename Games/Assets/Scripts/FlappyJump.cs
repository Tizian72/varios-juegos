using UnityEngine;

public class FlappyJump : MonoBehaviour
{
    public float jumpForce = 5f;            // Fuerza de salto del personaje
    public int maxJumpCount = 2;            // Cantidad m�xima de saltos permitidos
    private int jumpCount = 0;              // Contador de saltos realizados

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // Verificar si se presiona la tecla de espacio o se toca la pantalla en dispositivos m�viles
        if (Input.GetKeyDown(KeyCode.Space) || Input.touchCount > 0)
        {
            Jump();
        }
    }

    private void Jump()
    {
        // Comprobar si a�n se permite saltar
        if (jumpCount < maxJumpCount)
        {
            rb.velocity = Vector3.up * jumpForce;
            jumpCount++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Verificar si colisionamos con el suelo, el techo o las paredes en movimiento
        if (collision.gameObject.CompareTag("Ground") ||
            collision.gameObject.CompareTag("Ceiling") ||
            collision.gameObject.CompareTag("MovingWall"))
        {
            // Aqu� puedes agregar la l�gica para perder el juego
            Debug.Log("Has perdido.");
        }
        else
        {
            // Reiniciar el contador de saltos si colisionamos con cualquier otro objeto
            jumpCount = 0;
        }
    }
}