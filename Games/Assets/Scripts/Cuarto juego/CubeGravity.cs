using UnityEngine;

public class CubeGravity : MonoBehaviour
{
    private Rigidbody rb;

    public Color defaultColor = Color.blue; // Color predeterminado del cubo

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false; // Desactivamos la gravedad inicialmente

        Renderer renderer = GetComponent<Renderer>();
        renderer.material.color = defaultColor; // Establecemos el color predeterminado

        if (defaultColor == Color.red)
        {
            rb.velocity = Vector3.up * 2f; // Establece la gravedad hacia arriba
        }
        else if (defaultColor == Color.blue)
        {
            rb.velocity = Vector3.down * 2f; // Establece la gravedad hacia abajo
        }
    }

    private void OnMouseDown()
    {
        ChangeColor();
    }

    private void ChangeColor()
    {
        Renderer renderer = GetComponent<Renderer>();
        Color currentColor = renderer.material.color;

        if (currentColor == Color.red)
        {
            renderer.material.color = Color.blue;
            rb.velocity = Vector3.down * 2f; // Establece la gravedad hacia abajo
        }
        else if (currentColor == Color.blue)
        {
            renderer.material.color = Color.red;
            rb.velocity = Vector3.up * 2f; // Establece la gravedad hacia arriba
        }
    }
}