using UnityEngine;

public class MovingWall : MonoBehaviour
{
    public GameObject wallPrefab;        // Prefab del muro
    public float wallSpeed = 5f;         // Velocidad de movimiento del muro
    public float spawnInterval = 2f;     // Intervalo de tiempo entre la aparici�n de muros
    public Transform player;             // Referencia al transform del jugador

    private void Start()
    {
        // Iniciar la generaci�n continua de muros
        InvokeRepeating("SpawnWall", 0f, spawnInterval);
    }

    private void SpawnWall()
    {
        // Generar una posici�n aleatoria en el eje Y
        float spawnPosY = Random.Range(-2.5f, 2.5f);
        Vector3 spawnPos = new Vector3(transform.position.x, spawnPosY, transform.position.z);

        // Instanciar el muro en la posici�n generada
        GameObject wall = Instantiate(wallPrefab, spawnPos, Quaternion.identity);

        // Aplicar movimiento de derecha a izquierda al muro
        Rigidbody wallRigidbody = wall.GetComponent<Rigidbody>();
        wallRigidbody.velocity = Vector3.left * wallSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto que colision� es el jugador
        if (other.transform == player)
        {
            // El jugador ha tocado un muro, perder partida
            Debug.Log("Has perdido la partida");
            // Aqu� puedes agregar tu l�gica para perder la partida, por ejemplo, reiniciar el nivel o mostrar un mensaje de game over.
        }
    }
}