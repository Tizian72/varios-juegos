using UnityEngine;

public class MoverPelota : MonoBehaviour
{
    public float velocidad = 5f; // Velocidad de movimiento de la pelota
    public float rangoHorizontal = 5f; // Rango horizontal en el que se mover� la pelota

    private bool moverDerecha = true; // Indica la direcci�n actual de movimiento

    private void Update()
    {
        // Calcula el desplazamiento en el eje X
        float desplazamientoX = velocidad * Time.deltaTime;

        // Determina la direcci�n de movimiento
        if (moverDerecha)
        {
            transform.Translate(Vector3.right * desplazamientoX);
        }
        else
        {
            transform.Translate(Vector3.left * desplazamientoX);
        }

        // Comprueba si se alcanza el rango horizontal y cambia la direcci�n de movimiento
        if (transform.position.x >= rangoHorizontal)
        {
            moverDerecha = false;
        }
        else if (transform.position.x <= -rangoHorizontal)
        {
            moverDerecha = true;
        }
    }
}
