using UnityEngine;
using UnityEngine.UI;

public class Llegada : MonoBehaviour
{
    public Transform jugador; // Referencia al transform del jugador
    public float alturaCaida = -5f; // Altura a la que se considera que el jugador ha ca�do
    public Text textoCaida; // Referencia al componente de texto

    private bool jugadorCayo = false; // Indica si el jugador ha ca�do

    private void Update()
    {
        // Comprueba si el jugador ha ca�do
        if (!jugadorCayo && jugador.position.y <= alturaCaida)
        {
            jugadorCayo = true;
            MostrarTexto();
        }
    }

    private void MostrarTexto()
    {
        // Activa el componente de texto y muestra el mensaje
        textoCaida.gameObject.SetActive(true);
        textoCaida.text = "Llegaste!!";
    }
}
