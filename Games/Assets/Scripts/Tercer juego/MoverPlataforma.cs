using UnityEngine;

public class MoverPlataforma : MonoBehaviour
{
    private bool isDragging = false;
    private float mouseStartY;
    private float objectStartY;

    private void OnMouseDown()
    {
        isDragging = true;
        mouseStartY = Input.mousePosition.y;
        objectStartY = transform.position.y;
    }

    private void OnMouseUp()
    {
        isDragging = false;
    }

    private void OnMouseDrag()
    {
        if (isDragging)
        {
            float mouseY = Input.mousePosition.y;
            float deltaY = mouseY - mouseStartY;
            float newObjectY = objectStartY + (deltaY * 0.01f); // Ajusta la velocidad de movimiento aqu�

            // Restringe el movimiento dentro de los l�mites deseados, si es necesario
            // Puedes utilizar condicionales para controlar los l�mites

            transform.position = new Vector3(transform.position.x, newObjectY, transform.position.z);
        }
    }
}
